export const Task = ({ task, onDelete, onToggle}) => {
  return (
    <div className={`task ${task.reminder ? 'reminder': ''}`} onDoubleClick={()=>onToggle(task.id)} >
      <h3>
        {task.text}{" "}
        <i
          style={{ color: "red" }}
          className="fas fa-trash-alt"
          onClick={() => onDelete(task.id)}
        ></i>{" "}
      </h3>
      <p>{task.day}</p>
    </div>
  );
};
