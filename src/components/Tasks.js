import { Task } from "./Task"

export const Tasks = ({tasks, onDelete, onToggle }) => {
  const tasklist = tasks.map((task)=>{
    return(
      <div key={task.id}>
        <Task task={task} onDelete={onDelete} onToggle={onToggle}  />
      </div>
    )
  })
  return (
      [tasklist]
  )
}
