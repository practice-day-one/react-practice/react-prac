import React from 'react';

function Person({person}) {
    const {name, age} = person;
    return (
        <div>
            <h1>{name}</h1>
            <h2>{age}</h2>
            
        </div>
    );
}

export default Person;