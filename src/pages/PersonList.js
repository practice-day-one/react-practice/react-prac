import React from 'react';
import Person from '../components/Person';

import user from '../data/users';

function PersonList() {
    const personlist = user.map((person) => {
        return(
             <Person key={person.id} person={person} />
        );
            
    })
    return (
        <div>
            {personlist};
        </div>
    );
}

export default PersonList;